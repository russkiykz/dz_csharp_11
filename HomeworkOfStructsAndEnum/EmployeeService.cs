﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace HomeworkOfStructsAndEnum
{
    public class EmployeeService : IComparer<Employee>
    {
        public static Employee[] ArrayCreation()
        {
            Console.Write("Введите размер массива: ");
            int.TryParse(Console.ReadLine(), out int size);
            if (size > 0)
            {
                Employee[] employees = new Employee[size];
                for(int i = 0; i < size; i++)
                {
                    Console.WriteLine($"Сотрудник №{i + 1}");
                    Console.Write("Введите ФИО: ");
                    var fullName = Console.ReadLine();
                    Console.Write("\nВыберите должность:\n1.Manager\n2.Boss\n3.Clerk\n4.Salesman\nВыбор: ");
                    var vacancy = Vacancies.Manager;
                    switch (Console.ReadLine())
                    {
                        case "1":
                            vacancy = Vacancies.Manager;
                            break;
                        case "2":
                            vacancy = Vacancies.Boss;
                            break;
                        case "3":
                            vacancy = Vacancies.Clerk;
                            break;
                        case "4":
                            vacancy = Vacancies.Salesman;
                            break;
                        default:
                            throw new Exception("Отсутствует должность из списка");
                    }
                    Console.Write("\nВведите заработную плату: ");
                    int.TryParse(Console.ReadLine(), out int salary);
                    Console.WriteLine("\nВведите дату принятия на работу");
                    Console.Write("День: ");
                    int.TryParse(Console.ReadLine(), out int day);
                    Console.Write("Месяц: ");
                    int.TryParse(Console.ReadLine(), out int month);
                    Console.Write("Год: ");
                    int.TryParse(Console.ReadLine(), out int year);
                    employees[i] = new Employee(fullName, vacancy, salary, new DateTime(year, month, day));
                }
                return employees;
            }
            else
            {
                throw new Exception("Array size error");
            }
            

            
        }
        public static void EmployeeInformation(Employee[] employees)
        {
            for (int i = 0; i < employees.Length; i++)
            {
                Console.WriteLine($"\nСотрудник №{i + 1}");
                Console.WriteLine($"ФИО: {employees[i].FullName}\nДолжность: {employees[i].Vacancies}\nЗаработная плата: {employees[i].Salary}\nДата приема на работу: {employees[i].EmploymentDate.ToShortDateString()}");
                Console.WriteLine("------------");
            }
        }

        public int Compare([AllowNull] Employee x, [AllowNull] Employee y)
        {
            return x.FullName.CompareTo(y.FullName);
        }

        public static Employee[] SortToFullName(Employee[] employees)
        {
            Array.Sort(employees, new EmployeeService());
            return employees;
        }

        public static void ManagerInformation(Employee[] employees)
        {
            SortToFullName(employees);
            int sumSalaryClerk = 0;
            int numberOfClerk = 0;
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i].Vacancies == Vacancies.Clerk)
                {
                    sumSalaryClerk += employees[i].Salary;
                    numberOfClerk++;
                }
            }
            double avgSalaryClerk = sumSalaryClerk / numberOfClerk;
            int number = 0;
            for (int i = 0; i < employees.Length; i++)
            {
                if(employees[i].Vacancies == Vacancies.Manager && employees[i].Salary > avgSalaryClerk)
                {
                    Console.WriteLine($"\nСотрудник №{number + 1}");
                    Console.WriteLine($"ФИО: {employees[i].FullName}\nДолжность: {employees[i].Vacancies}\nЗаработная плата: {employees[i].Salary}\nДата приема на работу: {employees[i].EmploymentDate.ToShortDateString()}");
                    Console.WriteLine("------------");
                    number++;
                }
            }
        }

        public static void InformationEmploymentDateFromBoss(Employee[] employees)
        {
            SortToFullName(employees);
            DateTime dateTime=DateTime.Now;
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i].Vacancies == Vacancies.Boss)
                {
                    dateTime = employees[i].EmploymentDate;
                    break;
                }
            }
            int number = 0;
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i].EmploymentDate>dateTime)
                {
                    Console.WriteLine($"\nСотрудник №{number + 1}");
                    Console.WriteLine($"ФИО: {employees[i].FullName}\nДолжность: {employees[i].Vacancies}\nЗаработная плата: {employees[i].Salary}\nДата приема на работу: {employees[i].EmploymentDate.ToShortDateString()}");
                    Console.WriteLine("------------");
                    number++;
                }
            }
        }
    }
}
