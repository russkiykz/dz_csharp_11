﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOfStructsAndEnum
{
    public struct Student
    {
        public string FullName { get; set; }
        public string Group { get; set; }
        public double AvgGrade { get; set; }
        public double Income { get; set; }
        public Gender Gender { get; set; }
        public FormOfEducation FormOfEducation { get; set; }

        public Student(string fullName,string group,double avgGrade,double income,Gender gender,FormOfEducation formOfEducation)
        {
            FullName = fullName;
            Group = group;
            AvgGrade = avgGrade;
            Income = income;
            Gender = gender;
            FormOfEducation = formOfEducation;
        }
    }
}
