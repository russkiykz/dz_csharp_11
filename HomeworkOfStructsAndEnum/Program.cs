﻿using System;

namespace HomeworkOfStructsAndEnum
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Задание 1.
                Console.WriteLine("Задание №1");
                Employee[] employees = EmployeeService.ArrayCreation();
                // a)
                Console.WriteLine("Пункт a)");
                EmployeeService.EmployeeInformation(employees);
                // b)
                Console.WriteLine("Пункт b)");
                EmployeeService.ManagerInformation(employees);
                //c)
                Console.WriteLine("Пункт c)");
                EmployeeService.InformationEmploymentDateFromBoss(employees);

                // Задание 2.
                Console.WriteLine("Задание №2");
                Student[] students = new Student[] { new Student("Болтушкин Никита","SEP-201",3.4,44000,Gender.Male,FormOfEducation.FullTime),
                                                     new Student("Иванов Иван","SEP-201",2.4,20000,Gender.Male,FormOfEducation.FullTime),
                                                     new Student("Сидорова Катерина","SEP-201",4.3,120000,Gender.Female,FormOfEducation.Extramural),
                                                     new Student("Касаткина Марина","SEP-201",3.2,10000,Gender.Female,FormOfEducation.Remote)};

                StudentService.PriorityList(students);
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
           
        }
    }
}
