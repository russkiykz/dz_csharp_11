﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOfStructsAndEnum
{
    public enum FormOfEducation
    {
        FullTime,
        Extramural,
        Remote
    }
}
