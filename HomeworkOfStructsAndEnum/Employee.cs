﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace HomeworkOfStructsAndEnum
{
    public struct Employee
    {
        public string FullName { get; set; }
        public Vacancies Vacancies { get; set; }
        public int Salary { get; set; }
        public DateTime EmploymentDate { get; set; }

        public Employee(string fullName,Vacancies vacancies,int salary, DateTime employmentDate)
        {
            FullName = fullName;
            Vacancies = vacancies;
            Salary=salary;
            EmploymentDate = employmentDate;
        }
    }
}
