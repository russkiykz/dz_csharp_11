﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace HomeworkOfStructsAndEnum
{
    public class StudentService : IComparer<Student>
    {
        public int Compare([AllowNull] Student x, [AllowNull] Student y)
        {
            if (x.Income < 42500)
            {
                return x.Income.CompareTo(y.Income);
            }
            else
            {
                return x.AvgGrade.CompareTo(y.AvgGrade);
            }
        }

        public static Student[] SortToPriorityList(Student[] students)
        {
            Array.Sort(students, new StudentService());
            return students;
        }

        public static void PriorityList(Student[] students)
        {
            SortToPriorityList(students);
            foreach(Student student in students)
            {
                Console.WriteLine($"ФИО: {student.FullName}\nГруппа: {student.Group}\nСредняя оценка: {student.AvgGrade}\nДоход на члена семьи: {student.Income}");
                Console.WriteLine("-----");
            }

        }

        
    }
}
